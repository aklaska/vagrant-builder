# -*- mode: ruby -*-
# vi: set ft=ruby :
######################################################################
# Customisation. Change variables here!
#
# 1. Operating system variables
#
# hostonly_network_ip_address: set this to an ip address that is not used on your machine.
#
hostonly_network_ip_address = ""
#
# My Oracle Support credentials: this is used to download patches and software as patch if that exists.
#
mos_username = ""
mos_password = ""
#
# database_name (empty creates no database)
#
database_name = ""
#
# global password: this is the password for sys, system, dbsnmp, asmsyspassword.
#
global_password = "oracle"
#
# filesystem type for u01 and oradata (when not using ASM)
# please mind OL5 does not contain the xfs and ext4 filesystems.
#
filesystem = "xfs"
#filesystem = "ext4"
#filesystem = "ext3"
#
# vagrant linux boxes by FritsHoogland
#
#linux = "FritsHoogland/oracle-8.3"
#linux = "FritsHoogland/oracle-8.2"
#linux = "FritsHoogland/oracle-8.1"
#linux = "FritsHoogland/oracle-8.0"
linux = "FritsHoogland/oracle-7.9"
#linux = "FritsHoogland/oracle-7.8"
#linux = "FritsHoogland/oracle-7.7"
#linux = "FritsHoogland/oracle-6.10"
#
# stage directory
#
stage_directory="/u01/install"
#
# virtual machine number of cpus.
#
vm_cpus = "4"
#
# virtual machine memory.
#
vm_memory = "6144"
#
# hostname (without .local)
#
vm_hostname = "oracle-database"
#
# does perl bundled with oracle segfault? 
#
perl_l4cache_workaround = "Y"
#
# asm_diskdiscoverystring_without_asterisk: this sets the ASM disk type
# (AFD is not implemented yet)
#
asm_diskdiscoverystring_without_asterisk = "/dev/oracle_asm_udev/"
#asm_diskdiscoverystring_without_asterisk = "ORCL:"
#asm_diskdiscoverystring_without_asterisk = "AFD:"
#
# selinux
#
selinux = "Y"
#selinux = "N"
#
########################################################################################
#
# 2. Database variables
#
# database characterset
#
database_characterset = "UTF8"
#
# size of online redologfiles in MB
#
redologfile_size = "100"
#
# pluggable_database: set to Y or N
#
pluggable_database = "N"
#pluggable_database = "Y"
#
# size of sga in MB
#
sga_target_mb=2000
#
# size of pga aggregate target in MB
#
pga_aggregate_target_mb=500
#
########################################################################################
# asm version to install, set empty for filesystem / no clusterware
# empty
asm_version="''"
#
# oracle 19.0.0.0
# WARNING: requires installation zip files to be placed in the ansible/files directory!
# the otn download/minimum version is 19.3.0.0.0
#asm_version="19.10.0.0.0"
#asm_version="19.9.0.0.0"
#asm_version="19.8.0.0.0"
#asm_version="19.7.0.0.0"
#asm_version="19.6.0.0.0"
#asm_version="19.5.0.0.0"
#asm_version="19.4.0.0.0"
#asm_version="19.3.0.0.0"
#asm_version="19.2.0.0.0" -- warning: requires sdc download, not available on otn 
#
# oracle 18.0.0.0
# WARNING: requires installation zip files to be placed in the ansible/files directory!
# the otn download/minimum version is 18.3.0.0.180717
#asm_version="18.13.0.0.210119"
#asm_version="18.12.0.0.201020"
#asm_version="18.11.0.0.200714"
#asm_version="18.10.0.0.200414"
#asm_version="18.9.0.0.200114"
#asm_version="18.8.0.0.191015"
#asm_version="18.7.0.0.190716"
#asm_version="18.6.0.0.190416"
#asm_version="18.5.0.0.190115"
#asm_version="18.4.0.0.181016"
#asm_version="18.3.0.0.180717"
# the oracle 18 versions below are not installable anymore because Oracle made 18.3 available only
# so do not use below versions
#asm_version="18.2.0.0.180417"
# no patches applied
#asm_version="18.0.0.0.0"
#
# oracle 12.2.0.1 
# WARNING: requires installation zip file to be placed in ansible/files directory!
#asm_version="12.2.0.1.210119"
#asm_version="12.2.0.1.201020"
#asm_version="12.2.0.1.200714"
#asm_version="12.2.0.1.200414"
#asm_version="12.2.0.1.200114"
#asm_version="12.2.0.1.191015"
#asm_version="12.2.0.1.190716"
#asm_version="12.2.0.1.190416"
#asm_version="12.2.0.1.190115"
#asm_version="12.2.0.1.181016"
#asm_version="12.2.0.1.180717" -- warning: doesn't exist
#asm_version="12.2.0.1.180417"
#asm_version="12.2.0.1.180116"
#asm_version="12.2.0.1.171121"
#asm_version="12.2.0.1.171017" -- warning: superseded 
#asm_version="12.2.0.1.170814"
#asm_version="12.2.0.1.170718"
#asm_version="12.2.0.1.170620"
#asm_version="12.2.0.1.170516" -- warning: withdrawn
# no patches applied
#asm_version="12.2.0.1.0"
#
# oracle 12.1.0.2
# the patch versions below are only available when extended support download access is granted
#asm_version="12.1.0.2.210119"
#asm_version="12.1.0.2.201020"
#asm_version="12.1.0.2.200714"
#asm_version="12.1.0.2.200414"
#asm_version="12.1.0.2.200114"
#asm_version="12.1.0.2.191015"
# the patches below are regularly available
#asm_version="12.1.0.2.190716"
#asm_version="12.1.0.2.190416"
#asm_version="12.1.0.2.190115"
#asm_version="12.1.0.2.181016"
#asm_version="12.1.0.2.180717"
#asm_version="12.1.0.2.180417"
#asm_version="12.1.0.2.180116"
#asm_version="12.1.0.2.171017"
#asm_version="12.1.0.2.170814"
#asm_version="12.1.0.2.170718"
#asm_version="12.1.0.2.170418"
#asm_version="12.1.0.2.170117"
#asm_version="12.1.0.2.161018"
#asm_version="12.1.0.2.160719"
#asm_version="12.1.0.2.160419"
#asm_version="12.1.0.2.160119"
#asm_version="12.1.0.2.5"
#asm_version="12.1.0.2.4"
#asm_version="12.1.0.2.3"
#asm_version="12.1.0.2.2"
#asm_version="12.1.0.2.1"
# no patches applied
#asm_version="12.1.0.2.0"
#
# oracle 12.1.0.1
# -- warning! does not work on OL7 
# the installation files must be downloaded from https://edelivery.oracle.com
# grid media are: V38501-01_1of2.zip and V38501-01_2of2.zip
#asm_version="12.1.0.1.160719"
#asm_version="12.1.0.1.160419"
#asm_version="12.1.0.1.160119"
#asm_version="12.1.0.1.9"
#asm_version="12.1.0.1.8"
#asm_version="12.1.0.1.7"
#asm_version="12.1.0.1.6"
#asm_version="12.1.0.1.5"
#asm_version="12.1.0.1.4"
#asm_version="12.1.0.1.3"
#asm_version="12.1.0.1.2"
#asm_version="12.1.0.1.1"
# no patches applied
#asm_version="12.1.0.1.0"
#
# oracle 11.2.0.4
# -- warning! does not work on OL7 without patch 18370031 applied before root.sh!
# the patch versions below are only available when extended support download access is granted
#asm_version="11.2.0.4.201020"
#asm_version="11.2.0.4.200714"
#asm_version="11.2.0.4.200414"
#asm_version="11.2.0.4.200114"
#asm_version="11.2.0.4.191015"
#asm_version="11.2.0.4.190716"
#asm_version="11.2.0.4.190416"
#asm_version="11.2.0.4.190115"
# the patches below are regularly available
#asm_version="11.2.0.4.181016"
#asm_version="11.2.0.4.180717"
#asm_version="11.2.0.4.180417"
#asm_version="11.2.0.4.180116"
#asm_version="11.2.0.4.171017"
#asm_version="11.2.0.4.170814"
#asm_version="11.2.0.4.170718"
#asm_version="11.2.0.4.170418"
#asm_version="11.2.0.4.170117" -- no psu update (so 161018 used)
#asm_version="11.2.0.4.161018"
#asm_version="11.2.0.4.160719"
#asm_version="11.2.0.4.160419"
#asm_version="11.2.0.4.160119"
#asm_version="11.2.0.4.8"
#asm_version="11.2.0.4.7"
#asm_version="11.2.0.4.6"
#asm_version="11.2.0.4.5"
#asm_version="11.2.0.4.4"
#asm_version="11.2.0.4.3"
#asm_version="11.2.0.4.2"
#asm_version="11.2.0.4.1" -- warning: doesn't exist
# no patches applied
#asm_version="11.2.0.4.0"
#
############################################################################
# database version to install, set empty for no database install
# empty
database_version="''"
#
# oracle 19.0.0.0
# WARNING: requires installation zip files to be placed in the ansible/files directory!
# the otn download/minimum version is 19.3.0.0.0
#database_version="19.10.0.0.0"
#database_version="19.9.0.0.0"
#database_version="19.8.0.0.0"
#database_version="19.7.0.0.0"
#database_version="19.6.0.0.0"
#database_version="19.5.0.0.0"
#database_version="19.4.0.0.0"
#database_version="19.3.0.0.0"
#database_version="19.2.0.0.0" -- warning: requires sdc download, not available on otn
#
# oracle 18.0.0.0
# WARNING: requires installation zip files to be placed in the ansible/files directory!
# the otn download/minimum version is 18.3.0.0.180717
#database_version="18.13.0.0.210119"
#database_version="18.12.0.0.201020"
#database_version="18.11.0.0.200714"
#database_version="18.10.0.0.200414"
#database_version="18.9.0.0.200114"
#database_version="18.8.0.0.191015"
#database_version="18.7.0.0.190716"
#database_version="18.6.0.0.190416"
#database_version="18.5.0.0.190115"
#database_version="18.4.0.0.181016"
#database_version="18.3.0.0.180717"
# the oracle 18 versions below are not installable anymore because Oracle made 18.3 available only
# so do not use below versions
#database_version="18.2.0.0.180417"
# no patches applied
#database_version="18.0.0.0.0"
#
# oracle 12.2.0.1 
# WARNING: requires installation zip files to be placed in the ansible/files directory!
#database_version="12.2.0.1.210119"
#database_version="12.2.0.1.201020"
#database_version="12.2.0.1.200714"
#database_version="12.2.0.1.200414"
#database_version="12.2.0.1.200114"
#database_version="12.2.0.1.191015"
#database_version="12.2.0.1.190716"
#database_version="12.2.0.1.190416"
#database_version="12.2.0.1.190115"
#database_version="12.2.0.1.181016"
#database_version="12.2.0.1.180717"
#database_version="12.2.0.1.180417"
#database_version="12.2.0.1.180116"
#database_version="12.2.0.1.171121"
#database_version="12.2.0.1.171017"
#database_version="12.2.0.1.170814"
#database_version="12.2.0.1.170718"
#database_version="12.2.0.1.170620"
#database_version="12.2.0.1.170516" -- warning: withdrawn
# no patches applied
#database_version="12.2.0.1.0"
#
# oracle 12.1.0.2
# these patches are only available when extended support download access is granted
#database_version="12.1.0.2.210119"
#database_version="12.1.0.2.201020"
#database_version="12.1.0.2.200714"
#database_version="12.1.0.2.200414"
#database_version="12.1.0.2.200114"
#database_version="12.1.0.2.191015"
# these patches are regularly available
#database_version="12.1.0.2.190716"
#database_version="12.1.0.2.190416"
#database_version="12.1.0.2.190115"
#database_version="12.1.0.2.181016"
#database_version="12.1.0.2.180717"
#database_version="12.1.0.2.180417"
#database_version="12.1.0.2.180116"
#database_version="12.1.0.2.171017"
#database_version="12.1.0.2.170814"
#database_version="12.1.0.2.170718"
#database_version="12.1.0.2.170418"
#database_version="12.1.0.2.170117"
#database_version="12.1.0.2.161018"
#database_version="12.1.0.2.160719"
#database_version="12.1.0.2.160419"
#database_version="12.1.0.2.160119"
#database_version="12.1.0.2.5"
#database_version="12.1.0.2.4"
#database_version="12.1.0.2.3"
#database_version="12.1.0.2.2"
#database_version="12.1.0.2.1"
# no patches applied
#database_version="12.1.0.2.0"
#
# oracle 12.1.0.1
# the installation files must be downloaded from https://edelivery.oracle.com
# database media are: V38500-01_1of2.zip and V38500-01_2of2.zip
#database_version="12.1.0.1.160719"
#database_version="12.1.0.1.160419"
#database_version="12.1.0.1.160119"
#database_version="12.1.0.1.9"
#database_version="12.1.0.1.8"
#database_version="12.1.0.1.7"
#database_version="12.1.0.1.6"
#database_version="12.1.0.1.5"
#database_version="12.1.0.1.4"
#database_version="12.1.0.1.3"
#database_version="12.1.0.1.2"
#database_version="12.1.0.1.1"
# no patches applied
#database_version="12.1.0.1.0"
#
# oracle 11.2.0.4
# these patches are only available when extended support download access is granted
#database_version="11.2.0.4.201020"
#database_version="11.2.0.4.200714"
#database_version="11.2.0.4.200414"
#database_version="11.2.0.4.200114"
#database_version="11.2.0.4.191015"
#database_version="11.2.0.4.190716"
#database_version="11.2.0.4.190416"
#database_version="11.2.0.4.190115"
# these patches are regularly available
#database_version="11.2.0.4.181016"
#database_version="11.2.0.4.180717"
#database_version="11.2.0.4.180417"
#database_version="11.2.0.4.180116"
#database_version="11.2.0.4.171017"
#database_version="11.2.0.4.170814"
#database_version="11.2.0.4.170718"
#database_version="11.2.0.4.170418"
#database_version="11.2.0.4.170117" -- no psu update (so 161018 used), ojvm 170117 exists
#database_version="11.2.0.4.161018"
#database_version="11.2.0.4.160719"
#database_version="11.2.0.4.160419"
#database_version="11.2.0.4.160119"
#database_version="11.2.0.4.8"
#database_version="11.2.0.4.7"
#database_version="11.2.0.4.6"
#database_version="11.2.0.4.5"
#database_version="11.2.0.4.4"
#database_version="11.2.0.4.3"
#database_version="11.2.0.4.2"
#database_version="11.2.0.4.1"
# no patches applied
#database_version="11.2.0.4.0"
#
# oracle 11.2.0.3
#database_version="11.2.0.3.15"
#database_version="11.2.0.3.14"
#database_version="11.2.0.3.13"
#database_version="11.2.0.3.12"
#database_version="11.2.0.3.11"
#database_version="11.2.0.3.10"
#database_version="11.2.0.3.9"
#database_version="11.2.0.3.8"
#database_version="11.2.0.3.7"
#database_version="11.2.0.3.6"
#database_version="11.2.0.3.5"
#database_version="11.2.0.3.4"
#database_version="11.2.0.3.3"
#database_version="11.2.0.3.2"
#database_version="11.2.0.3.1"
# no patches applied
#database_version="11.2.0.3.0"
#
# oracle 11.2.0.2
#database_version="11.2.0.2.12"
#database_version="11.2.0.2.11"
#database_version="11.2.0.2.10"
#database_version="11.2.0.2.9"
#database_version="11.2.0.2.8"
#database_version="11.2.0.2.7"
#database_version="11.2.0.2.6"
#database_version="11.2.0.2.5"
#database_version="11.2.0.2.4"
#database_version="11.2.0.2.3"
#database_version="11.2.0.2.2"
#database_version="11.2.0.2.1"
# no patches applied
#database_version="11.2.0.2.0"
#
# oracle 11.2.0.1
# WARNING: requires installation zip files to be placed in the ansible/files directory!
#database_version="11.2.0.1.6"
#database_version="11.2.0.1.5"
#database_version="11.2.0.1.4"
#database_version="11.2.0.1.3"
#database_version="11.2.0.1.2"
#database_version="11.2.0.1.1"
# no patches applied
#database_version="11.2.0.1.0"
#
# If you need even older versions, look at the following projects:
# oracle 10.2.0.1: https://gitlab.com/FritsHoogland/ol511_oracle102.git 
# oracle 9.2.0.4: https://gitlab.com/FritsHoogland/ol48_oracle92.git
######################################################################
#
Vagrant.configure("2") do |config|
  # see box selection above
  config.vm.box = "#{linux}"
  # synced folder, enabled by default, this is specific a compiled kernel module for the installed kernel!
  # the FritsHoogland boxes have the virtualbox vboxsf filesystem kernel module compiled for the current virtualbox version for the default kernel
  # this means that if the synced folder fails because of a different virtualbox or kernel version, you have to install the guest additions yourself
  # look at: https://www.if-not-true-then-false.com/2010/install-virtualbox-guest-additions-on-fedora-centos-red-hat-rhel/ 
  #config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder ".", "/vagrant", mount_options: [ "fmode=755" ]
  # insert private key. this fails if the LOCAL drive cannot set sufficient protective rights (like VFAT)
  #config.ssh.insert_key = false
  #config.ssh.private_key_path = [ "~/.vagrant_ssh/id_dsa", "~/.vagrant.d/insecure_private_key" ]
  # this is the hostname set in the VM
  config.vm.hostname = "#{vm_hostname}.local"
  # this is a static ip address set, so you can use it from your machine or another VM
  config.vm.network "private_network", ip: "#{hostonly_network_ip_address}"
  # boot timeout 
  config.vm.boot_timeout = 60
  config.vm.provider "virtualbox" do |vb|
    # virtual machine memory setting 
    vb.memory = "#{vm_memory}"
    # virtual machine number of virtual cpus
    vb.cpus = "#{vm_cpus}" 
    # workaround for a perl issue
    if "#{perl_l4cache_workaround}" == "Y"
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/Leaf', '0x4' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/SubLeaf', '0x4' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/eax', '0' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/ebx', '0' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/ecx', '0' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/edx', '0' ]
      vb.customize [ 'setextradata', :id, 'VBoxInternal/CPUM/HostCPUID/Cache/SubLeafMask', '0xffffffff' ]
    end
    # addition and check for extra disk for u01
    u01_disk = "u01_disk.vdi"
    if !File.exist?(u01_disk)
      # 40960 = 40G
      # 51200 = 50G
      vb.customize [ 'createhd', '--filename', u01_disk, '--size', 51200 ]
    end
    vb.customize [ 'storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', u01_disk ]
    # additiona and check for extra disk for data. this is used either as a filesystem or as a block device for ASM
    data_disk = "data_disk.vdi"
    if !File.exist?(data_disk)
      # 40960 = 40G
      vb.customize [ 'createhd', '--filename', data_disk, '--size', 40960 ]
    end
    vb.customize [ 'storageattach', :id, '--storagectl', 'SATA Controller', '--port', 3, '--device', 0, '--type', 'hdd', '--medium', data_disk ]
  end
  # provisioning 
  # first install git so we can use git clone the oracle-database-setup repository
  config.vm.provision "install git", type: "shell", privileged: true, inline: "yum -y install git"
  # git clone oracle-database-repository
  config.vm.provision "clone oracle-database-setup", type: "shell", privileged: false, inline: "[ -d oracle-database-setup ] || git clone https://gitlab.com/FritsHoogland/oracle-database-setup.git"
  # in case you can't use the virtualbox shared directory, you can place your install files in the files directory, which gets copied to /tmp
  config.vm.provision "copy files", type: "file", source: "files", destination: "/tmp"
  # if the copied files happened to be readable only to the owner, change them to world readable
  config.vm.provision "make vagrant files in /tmp readable", type: "shell", privileged: false, inline: "find /tmp -type f -user vagrant -exec chmod o+r \{\} \\; | true"
  # the playbook to setup oracle is run using a settings file in the VM, which reflects the settings made above.
  config.vm.provision "create settings file", type: "shell", privileged: false, inline: <<-SETTINGS_FILE
    echo '---
    mosuser: #{mos_username}
    host_set_ip_address: #{hostonly_network_ip_address}
    oracle_base: /u01/app/oracle
    database_name: #{database_name}
    global_password: #{global_password}
    pga_aggregate_target_mb: #{pga_aggregate_target_mb}
    sga_target_mb: #{sga_target_mb}
    asm_create_file_dest: DATA
    asm_device: sdc
    db_create_file_dest: /u01/app/oracle/oradata
    asm_diskdiscoverystring_without_asterisk: #{asm_diskdiscoverystring_without_asterisk}
    linux: #{linux}
    asm_version: #{asm_version}
    database_version: #{database_version}
    stage_directory: #{stage_directory}
    filesystem: #{filesystem}
    database_characterset: #{database_characterset}
    redologfile_size: #{redologfile_size}
    pluggable_database: #{pluggable_database}
    selinux: #{selinux}
    ' > oracle-database-setup/vars/settings.yml
    SETTINGS_FILE
  # in order to make the MOS password not easily readable, use ansible vault to store it encrypted
  config.vm.provision "create encrypted vars/mospass.yml file (installs ansible)", type: "ansible_local" do |ansible|
    ansible.provisioning_path = "/home/vagrant/oracle-database-setup"
    ansible.extra_vars = { password: "#{mos_password}" }
    ansible.limit = "all, localhost"
    ansible.playbook = "encrypt_mospass.yml"
  end
  # run the setup playbook to install oracle
  config.vm.provision "install oracle software", type: "ansible_local" do |ansible|
    ansible.provisioning_path = "/home/vagrant/oracle-database-setup"
    ansible.limit = "all, localhost"
    ansible.playbook = "setup.yml"
  end
end
